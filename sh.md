# POSIX Shell Scripting Style Guide

v0.0.0-draft-r1

Please note that if you are not exclusively using
POSIX shell features only, you are probably better off
using the syntax and style guide for your respective shell
and while much of this style guide transfers to other
shell languages, there are many important key differences.

## Table of Contents
- [When to use shell](#when-to-use-shell)
- [When to use POSIX](#when-to-use-posix)
- [Code Layout](#code-layout)
	+ [Indentation](#indentation)
	+ [Line length](#line-length)
- [Variables](#variables)
- [Quoting](#quoting)
- [Comments](#comments)
	+ [Multiline comments](#multiline-comments)
	+ [Inline comments](#inline-comments)
	+ [Documention comments](#documention-comments)
- [Shebang and file extension](#shebang-and-file-extension)
- [Redirections](#redirections)
- [Pipelines](#pipelines)
- [Control flow](#control-flow)
	+ [If statement](#if-statement)
	+ [Short circuiting](#short-circuiting)
	+ [Case statement](#case-statement)
	+ [Loops](#loops)
- [Functions](#functions)
- [Error handling](#error-handling)
- [Naming Conventions](#naming-conventions)
	+ [Variable names](#variable-names)
	+ [Function names](#function-names)
- [Commands](#commands)
- [References](#references)
- [License](#license)

## When to use shell

Shell should primarily be used for small utilities or
simple wrapper scripts of larger programs that expose a
command-line interface (CLI).

Do use shell:
- When you do not know which scripting language is available in the target Unix-like system. A shell is ubiquitous.
- For quickly and easily combining functionality of CLI programs you do not develop.
- When time matters. Often there are already specialized CLI programs that do what you want and you only need to combine them.

Do not use shell:
- When performance matters.
- As the main or only way to combine a program you develop with another you do not
  - If both programs were written in the same language, it would be better to make libraries for that language: APIs; not only CLIs.
  - If both programs were written in different languages, it would be better if one was compiled to a system library (a.k.a. "C" library, DLL, .so, .dylib) so that it could be used from the other language via methods like FFI

## When to use POSIX

Do use POSIX shell:
- When seeking portability to Unix-like systems from at least the mid 1990's to the present.
- When the scripts are fairly straightforward.

Do not use POSIX shell:
- When robustness matters
  - Unless using the `modernish` framework
- When seeking for maximum portability to ancient pre-POSIX systems with Bourne-derived shells such as BSD, SunOS or SysV-derived 1980's systems.

"Portable" is a subjective word, it means different things
to different people in different situations. For some, it
may mean that a script can run in all of their Linux
systems with recent versions of shells, having installed,
or not, additional packages. Others may include recent
versions of macOS, Android's system shell or Termux,
FreeBSD, OpenBSD, etc.

The POSIX Shell Command Language is one that has barely
changed at all since its first publication in 1992,
predating any reminiscent form of the most popular
Unix-like operating systems of today and where any system
at the time would be considered ancient today. And yet, it
is still a common denominator.

This guide is not for the ones that look for maximum
portability to ancient pre-POSIX systems with
Bourne-derived shells such as BSD, SunOS or SysV-derived
1980's systems.

If you are using a single non-POSIX feature of a shell, you might as
well use more of them

Here is a summary of what major Unix-like platforms are using as their
default shells in their latest versions.

| Platform  | System shell `/bin/sh`  | User shell |
|-----------|-------------------------|------------|
| Debian    | `dash`                  | `bash`     |
| Ubuntu    | `dash`                  | `bash`     |
| Fedora    | `bash`                  | `bash`     |
| RHEL      | `bash`                  | `bash`     |
| Arch      | `bash`                  | `bash`     |
| Alpine    | `ash` (busybox)         | `ash`      |
| Android*  | `mksh`                  |            |
| macOS*    | `bash`                  | `zsh`      |
| FreeBSD   | `ash` (fork)            | `ash`      |
| OpenBSD   | `ksh` (pdksh fork)      |            |
| NetBSD    | `ash` (fork)            | `ash`      |

* On Android, the system shell goes in `/system/bin/sh`, not `/bin/sh`
* On macOS, `/bin/sh` is a wrapper that points to `/private/var/select/sh` or falls back to `bash`, `dash` or `zsh`

<!-- TODO: include MSYS2 -->

<!-- TODO: POSIX vs portability difference -->

## Code Layout

### Indentation

Use tabs characters insteaf of spaces. It immediately gives
support for indented heredoc syntax `<<-EOF`: leading tabs
get discarded and ending mark can be indented too.

Spaces should be used solely to remain consistent with
code that is already indented with spaces.

```sh
# with tabs
if :; then
	cat <<-EOF             # OK
		this is some tab
		indented text
	EOF
fi

# with spaces
if :; then
  cat <<EOF                # NO
this is some tab
indented text
EOF
fi
```

Do not tab-indent strings inside parameter expansion
even if inside a heredoc since leading tabs may not
be discarded.

```sh
cat <<-EOF                # OK
	${content:-"\
someline1
someline2"}
EOF

cat <<-EOF                # NO
	${content:-"\
	someline1
	someline2"}
EOF
```

### Line length

- Try to limit all lines to a maximum of 79 characters.
- Use backslashes `\` to wrap long lines, for example when using a command with many arguments
- Insert one indentation level when wrapping long lines

```sh
command1 --with --many --arguments --that \
	--may --not --fit --in --one --line \
	-- and has to be splitted into many
```

## Variables

Multiple variables can be assigned on the same
line and is often more performant by implementations.
Backslashes can be used to separate assignments in
different lines if it makes it more readable.

```sh
a=10 b=20 c=30  # OK

a=10 \
b=20 \
c=30       # OK

a=10       # NO
b=20
c=30
```

You should stick to this unless there is a good
reason to include an inline comment on variable
definition.

<!-- Do not use `readonly` -->
<!-- Prefer putting constants at the beginning of the file -->

```sh
: "${var:="default"}"      # OK
var="${var:-"default"}"    # NO
```

## Quoting

For string literals:
- By default use single quotes
- Do not quote strings that are integers
- Use double quotes only if it makes a string with single quotes more readable (does not need to escape shell tokens)
- Single quote empty strings on variable assignment
<!--- Do not quote first command -->

```sh
text='hello'                 # OK
text="hello"                 # NO
text='They said "hello"'     # OK
text="I'm here"              # OK
text='I'\''m here'           # NO
text=10                      # OK
text='10'                    # NO
text=''                      # OK
text=                        # NO
```

You can omit the parentheses only if it is an argument with
a single parameter expansion
Also quote in nested parameter expansions

```sh
printf -- '%s\n' "${var}"              # OK
printf -- '%s\n' "$var"                # OK
printf -- '%s\n' \$var                 # NO
printf -- '%s\n' "${var%"${var#?}"}"   # OK
printf -- '%s\n' "${var%${var#?}}"     # NO
```

Note that using braces in `${var}` is not a form of quoting.
"Double quotes" must be used arount it as well.

For more complex quoting, usually using a mix between single
and double quoting is more readable than trying to use
only double quoting with multiple escaping `\` or using
single quoting with multiple `'\''`

<!-- TODO: heredocs, when to use them -->

## Comments

### Multiline comments

Do not abuse heredocs as a way to emulate multiline comments,
this is inefficient and makes it harder for linting tools.

```sh
# These are some       # OK
# commmented lines

: <<COM                # NO
These is a heredoc
attempting to be
a multiline comment
COM
```

### Inline comments

Use inline comments sparingly.

An inline comment is a comment on the same line as a statement.
Inline comments should be separated by at least two spaces from
the statement. They should start with a `#` and a single space.

```sh
var='hello'  # OK
var='hello' #        NO
```

### Documention comments

A SCCS-style comment `@(#)` is recommended to be put
at the beginning of a file so that a system with the
`what` command can see a brief description of what
the file does.

```sh
#!/bin/sh

# @(#) This is a script that does xyz
```

Function documention comments should go before a function
name, not inside the function body

```sh
# This function does xyz          # OK
fn1() {
	…
}

fn1() {
	# This function does xyz      # NO
	…
}
```

You then may use a documentation generator for shell scripts
like [`shdoc`](https://github.com/reconquest/shdoc) or make a
doxygen filter.

## Shebang and file extension

Always put a shebang/hashbang line at the beginning of an
executable script with no spaces before or after the `#!`.

Not including a shebang line would make the script execute
with the current shell which could not be what one expects.

```sh
#!/bin/sh    # OK
  #!/bin/sh  # NO
#!  /bin/sh  # NO
```

The recommended shell is the system shell `/bin/sh` that should
be a POSIX compliant shell.

Remember, if you are actively using non-POSIX features, the
shebang should be set to the other shell (for example
`#!/usr/bin/env bash`) as early as possible.

If the file is a script supposed to be run by a regular user,
the extension `.sh` should be omitted. Libraries or developer
scripts should always have the `.sh` extension.

If you are actively using non-POSIX features, change the
extension to the other shell (for example `.bash`) as
early as possible.

<!-- TODO: about libraries in bindir for simple `. lib.sh` syntax-->

## Redirections

If you are redirecting a single command to standard error
put the redirection first, this makes it faster
to understand that it will not be the output of a
command substitution `$()`.

```sh
>&2 echo 'There was an error'   # OK
echo 'There was an error' >&2   # NO
```

To discard both standard output and error, do it this way:

```sh
command1 >/dev/null 2>&1    # OK
command1 2>/dev/null >&2    # NO
>/dev/null 2>&1 command1    # NO
```

### Here documents (heredocs)

Use `EOF` as the default delimiter word

## Pipelines

```sh
command1 | command2    # OK

# OK
command1 \
| command2 \
| command3 \
| command4
```

Reasoning: having a `|` at the beginning of an indented
line makes it immediately recognizable as a pipeline.
While if the `|` was at the end it would not need a `\`,
readers would have to look to the end of a line for
the pipe character; even if subsequent lines were
indented, the command name can be confused as an
argument to the first command in the pipeline at
first glance.

## Control flow

### If statement

- Use `[` instead of `test`
- Put `; then` on the same line as `if`
  - Except when the condition is a multiline compound command in braces `{ }`

<!--- Use the not operator before `[`-->

```sh
if [ 10 -eq 10 ]; then     # OK
	…
fi

if test 10 -eq 10; then    # NO
	…
fi

if [ 10 -eq 10 ]           # NO
then
	…
fi

if {                       # OK
	command1 && command2 \
	| command3
} then
	…
fi
```

### Short circuiting

Beware that `if`-`else` conditionals are ***NOT*** equivalent
to shortcircuiting `&&`-`||`. Let the expressions:

```sh
if cmd1; then cmd2; else cmd3; fi  # (a)
cmd1 && cmd2 || cmd3               # (b)
# which is the same as:
{ cmd1 && cmd2; } || cmd3          # (b)
```

On the `(a)` statement, the `cmd3` command will run only
if `cmd1` returns a non-zero exit status.

On the `(b)` statement, the `cmd3` command will run if
either `cmd1` or `cmd2` return a non-zero exit status.

Therefore, the statements will differ when `cmd1` succeeds
and `cmd2` fails; and in such case the `(b)` statement will
execute `cmd3` and return its exit status, while the `(a)`
statement will not, and return `cmd2`'s exit status instead.

Nevertheless, you can still use short-circuting as simple
conditional branches, but only in a linear conditional
tree for a single uncompounded command where the
expression after `&&` is assured to succeed. The moment
you need `{}`, write it as an `if` statement instead.

```sh
# One-liner
: &&  x='T' || x='F'               # OK
if :; then x='T'; else x='F'; fi   # NO
: && x='T' && x='TT'               # OK
: && x='T' || x='F' && x='FT'      # NO

# Multiline
if :; then        # OK
	x='T'
else
	x='F'
fi

: && {            # NO
	x='T'
} || {
	x='F'
}

# Elifs
if [ "$n" -lt 5 ]; then       # OK
	x='less than 5'
elif [ "$n" -lt 10 ]; then
	x='less than 10'
else
	x='more or equal than 10'
fi

[ "$n" -lt 5 ] && {           # NO!!
	x='less than 5'
} || { [ "$n" -lt 10 ] && {
	x='less than 10'
} || {
	x='greater or equal than 10'
} }
```

### Case statement

- Put case patterns and `;;` on the same indentation level as the `esac` keyword
- Write patterns with an initial `(`, without leading or traling whitespace
- Separate `|` with a space before and after
- Do not write `;;` for the final matching pattern

```sh
case "$var" in      # OK
(a)
	echo 'a'
;;
(b | c)
	echo 'b or c'
;;
(d) ;;
(*)
	echo 'none'
esac

case "$var" in      # NO
	a)
		echo 'a'
		;;
	b|c)
		echo 'b or c'
		;;
	d)
		;;
	*)
		echo 'none'
		;;
esac
```

Reasoning: `case` in shell script does not use braces unlike
other C derived languages, there is no reason to add one indentation
level for the same syntax branch, much less put `;;` in the
same level as commands in a matching branch.

Single commands may be put on the same line as the pattern
and `;;` as long as the expression remains readable, for
which aligning is also recommended.

```sh
while getopts 'abf:' opt; do
	case "${opt}" in
	(a) Aflag=1 ;;
	(b) Bflag=1 ;;
	(f) files="${OPTARG}"
	esac
done
```

### Loops

- Put `; do` on the same line as `for`, `while` or `until`
  - Except when the `while` or `until` condition is a compound command in braces `{ }`
  - Except when the `for` word list is multiline separated

```sh
for i in 'one' 'two' 'three'; do  # OK
	…
done

for i in 'one' 'two' 'three'   # NO
do
	…
done

# OK
for i in \
'one' \
'two' \
'three'
do
	…
done
```

When looping over the positional parameters `"$@"`,
omit the `in "$@";` as this is assumed.

<!--Note 89' ash does need a ';'-->

```sh
for arg do   # OK
	…
done

for arg in "$@"; do   # NO
	…
done
```

#### Looping in filepaths

```sh
find -name '*.sh' -exec sh -s {} + <<-EOF
	for file do
		…
	done
EOF
```

## Functions

- Write the first `{` on the same line as the function name
- Write `()` right after the function name
- Do not write functions as one-liners

```sh
fn1() {   # OK
	…
}

fn1 ()    # NO
{
	…
}

fn1(){ …;}  # NO
```

Use `errno` values values for your exit codes

Have a `main` function as

## Error handling

```sh
fn1() {
	may_fail || return $?
	…
}

if fn1; then
	…
else
	case $? in
	(1) … ;;
	(2) … ;;
	(*) …
	esac
fi
```

## Naming Conventions

### Variable names

On scripts:
- Snake-case for variables that may change
- Snake-case with first letter in upper-case for variables that should not change
- Snake-case with all-caps for environment variables or special shell variables

```sh
var_one='hello'  # variable that may change
Var_one='hello'  # variable that should not change ("constant")
VAR_ONE='hello'  # environment variable
```

Special shell variables: `REPLY`, `OPTARG`, `OPTIND`

On libraries:
- Prefix each variable with the library and module namespace with underscores: `_libraryname_`
- Library and module namespace do not need to follow a casing rule
- Camel-case for the actual variable name

```sh
_libname_varOne='hello'
_libname_mod1_varTwo='hello'
```

Reasoning: POSIX does not have the `local` keyword to keep
variables to a function scope on definition

### Function names

- Snake-case

```sh
fn_one() {
	…
}
```

## Commands

Do not use `echo` with anything other than no arguments or
a literal string that does not start with `-`.
Do not use it to print variables.

```sh
printf -- '%s\n' "$directory"  # OK
echo "$directory"              # NO
echo '/some/path'              # OK
```

Separate arguments from options with `--` in all common
Unix utilities that support it.

```sh
mkdir -p -- "$directory"      # OK
mkdir -p "$directory"         # NO
```

## References

- [POSIX shell specification](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html)
- [Modernish design notes](https://github.com/modernish/modernish/blob/master/share/doc/modernish/DESIGN.md)
- [Modernish codying style](https://github.com/modernish/modernish/blob/master/share/doc/modernish/CODINGSTYLE)
- [GNU autoconf Portable Shell](https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.71/autoconf.html#Portable-Shell)
- [Shellcheck Wiki](https://www.shellcheck.net/wiki/)
- [Style Guide for Python (PEP8)](https://peps.python.org/pep-0008)
- [Google's Bash Styleguide](https://google.github.io/styleguide/shellguide.html)

## License

© 2023. This style guide is licensed under the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
