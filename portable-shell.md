Only if you ***really*** need portability with *very* old systems
you can use this pre POSIX syntax:

```sh
if test x"$var" = x; then
	…
fi
```
