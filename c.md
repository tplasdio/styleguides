# C Style Guide

I follow a style similar to Suckless

In GNU `indent` terms:

```sh
indent \
  -nbad -bap -nbc -bbo -ncdb -nfc1 -nfca \
  -hnl -lp -nprs -saf -sai -saw -nsc -cs \
  -ip2 -ppi2 -psl -br -brs -ce -cli0 -d0 \
  -c33 -cd33 -npcs -sob -i4 -ts4 -l79 file.c
```

Additionally:
- No old-style function definitions

<!-- TODO: not align inline comments with tabs -->
