# Styleguides

- These are my personal coding style guides
- Coding style guides are meant to be opinionated and prescriptive

## Contribute

Open an issue on codeberg or send a merge request with `git bug`
with your issue.

## See also

https://github.com/Kristories/awesome-guidelines
